-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-02-2015 a las 05:56:12
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bddweb`
--
CREATE DATABASE IF NOT EXISTS `bddweb` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `bddweb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargaacademica`
--

DROP TABLE IF EXISTS `cargaacademica`;
CREATE TABLE IF NOT EXISTS `cargaacademica` (
  `idCargaAcademica` int(10) NOT NULL AUTO_INCREMENT,
  `idEstudiante` int(10) NOT NULL,
  `idMateria` int(10) NOT NULL,
  `Nota` int(2) NOT NULL,
  `Seccion` int(2) NOT NULL,
  PRIMARY KEY (`idCargaAcademica`),
  UNIQUE KEY `idCargaAcademica` (`idCargaAcademica`),
  KEY `FKCargaAcade564270` (`idMateria`),
  KEY `FKCargaAcade695130` (`idEstudiante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

DROP TABLE IF EXISTS `estudiante`;
CREATE TABLE IF NOT EXISTS `estudiante` (
  `idEstudiante` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Cedula` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `Direccion` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idTipoDeSangre` int(10) NOT NULL,
  `Rif` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NroPasaporte` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TelefonoLocal` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `TelefonoCelular` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaIngreso` date NOT NULL,
  PRIMARY KEY (`idEstudiante`),
  UNIQUE KEY `idEstudiante` (`idEstudiante`),
  KEY `FKEstudiante644752` (`idTipoDeSangre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

DROP TABLE IF EXISTS `materia`;
CREATE TABLE IF NOT EXISTS `materia` (
  `idMateria` int(10) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idMateria`),
  UNIQUE KEY `idMateria` (`idMateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referenciapersonal`
--

DROP TABLE IF EXISTS `referenciapersonal`;
CREATE TABLE IF NOT EXISTS `referenciapersonal` (
  `idReferenciaPersonal` int(10) NOT NULL AUTO_INCREMENT,
  `idEstudiante` int(10) NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Cedula` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `Telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idReferenciaPersonal`),
  UNIQUE KEY `idReferenciaPersonal` (`idReferenciaPersonal`),
  KEY `FKReferencia364832` (`idEstudiante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposangre`
--

DROP TABLE IF EXISTS `tiposangre`;
CREATE TABLE IF NOT EXISTS `tiposangre` (
  `idTipoSangre` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idTipoSangre`),
  UNIQUE KEY `idTipoSangre` (`idTipoSangre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cargaacademica`
--
ALTER TABLE `cargaacademica`
  ADD CONSTRAINT `FKCargaAcade695130` FOREIGN KEY (`idEstudiante`) REFERENCES `estudiante` (`idEstudiante`),
  ADD CONSTRAINT `FKCargaAcade564270` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`idMateria`);

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `FKEstudiante644752` FOREIGN KEY (`idTipoDeSangre`) REFERENCES `tiposangre` (`idTipoSangre`);

--
-- Filtros para la tabla `referenciapersonal`
--
ALTER TABLE `referenciapersonal`
  ADD CONSTRAINT `FKReferencia364832` FOREIGN KEY (`idEstudiante`) REFERENCES `estudiante` (`idEstudiante`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
